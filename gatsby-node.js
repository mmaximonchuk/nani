const path = require(`path`);
const { productTypes } = require("./src/constants/product-types");
const { ROUTES } = require("./src/constants/routes");

const catalogPath = path.resolve(`./src/templates/catalog.jsx`);

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  // Define a template for blog post
  const productDetails = path.resolve(`./src/templates/product-details.jsx`);

  const allProducts = await Promise.all(
    productTypes.map(async (productType) => {
      return await graphql(
        `
        {
          allPrismicProduct(filter: { data: { type: { eq: "${productType}" } } }) {
            nodes {
              data {
                card_image {
                  url
                }
                density {
                  text
                }
                gallery {
                  gallery_image {
                    url
                  }
                  color
                }
                material {
                  text
                }
                price {
                  text
                }
                title {
                  text
                }
                type
                is_in_black
                is_in_white
                anime
              }
              id
            }
          }
        }
      `
      );
    })
  );

  const products = allProducts.flatMap((item) => item.data.allPrismicProduct.nodes);

  products.forEach((product) => {
    createPage({
      path: `${ROUTES.PRODUCTS}/${product.id}`,
      component: productDetails,
      context: {
        product: { ...product.data, id: product.id },
      },
    });
  });

  console.log("allProducts: ", allProducts);
  productTypes.forEach((productType, index) => {
    createPage({
      path: `/catalog/${productType}`,
      component: catalogPath,
      context: {
        data: allProducts[index].data.allPrismicProduct.nodes,
        productType,
      },
    });
  });
};
