import * as React from "react";
import { CartProvider } from "react-use-cart";
import { PrismicPreviewProvider } from "gatsby-plugin-prismic-previews";

import { repositoryConfigs } from "./config/prismic/previews";
import ToastsProvider from "./src/context/ToastsProvider";

export const wrapRootElement = ({ element }) => (
  <PrismicPreviewProvider repositoryConfigs={repositoryConfigs}>
    <ToastsProvider>
      <CartProvider>{element}</CartProvider>
    </ToastsProvider>
  </PrismicPreviewProvider>
);

export const onRenderBody = ({ setHtmlAttributes }) => {
  setHtmlAttributes({ lang: "uk" });
};
