import React from "react";
import { Toaster } from "react-hot-toast";

const ToastsProvider = ({ children }) => {
  return (
    <div>
      {children}
      <Toaster
        toastOptions={{
          className: "custom-toasts",
        }}
        position="top-center"
        reverseOrder={false}
      />
    </div>
  );
};

export default ToastsProvider;
