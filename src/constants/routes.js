const ROUTES = {
  HOMEPAGE: "/",
  CART: "/cart",
  OLD_COLLECTIONS: "/old-collections",
  PRODUCTS: "/products",
  CATALOG: "/catalog",
  CATALOG_HOODIE: "/catalog/hoodie",
  CATALOG_TSHIRT: "/catalog/tshirt",
  ORDER: "/order",
  DELIVERY_AND_PAYMENT: "/delivery-and-payment",
  ABOUT: "/about",
  PAYMENT: "/payment",
  THANK_YOU: "/thank-you",
};

module.exports = { ROUTES };
