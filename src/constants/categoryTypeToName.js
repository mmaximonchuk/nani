const TSHIRT_PRODUCT_TYPE = "tshirt";
const HOODIE_PRODUCT_TYPE = "hoodie";

const categoryTypeToName = {
  [TSHIRT_PRODUCT_TYPE]: "Футболки",
  [HOODIE_PRODUCT_TYPE]: "Худі",
};

module.exports = {
  categoryTypeToName,
  TSHIRT_PRODUCT_TYPE,
  HOODIE_PRODUCT_TYPE,
};
