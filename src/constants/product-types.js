const { categoryTypeToName } = require("./categoryTypeToName");


const productTypes = Object.keys(categoryTypeToName);

const PRODUCT_COLORS_TO_NAMES = {
  "black": "Чорний",
  "white": "Білий",
}
const PRODUCT_COLORS = Object.keys(PRODUCT_COLORS_TO_NAMES);
const PRODUCT_SIZES = ["s", "m", "l", "xl"];


const ANIME_NAMES = {
  naruto: "Naruto",
  berserk: "Berserk",
  chainsaw_man: "Chainsaw man",
  demon_slayer: "Demon slayer",
  one_piece: "One piece",
  attack_on_titan: "Attack on Titan",
};

module.exports = {
  productTypes,
  PRODUCT_COLORS,
  PRODUCT_SIZES,
  ANIME_NAMES,
  PRODUCT_COLORS_TO_NAMES
};
