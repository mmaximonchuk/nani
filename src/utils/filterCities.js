const filterCities = (inputValue, cities) => {
  return cities.filter((city) =>
    city.label.toLowerCase().includes(inputValue.toLowerCase())
  );
};

module.exports = { filterCities };
