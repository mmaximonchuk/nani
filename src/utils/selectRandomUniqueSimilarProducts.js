const selectRandomUniqueSimilarProducts = (
  products,
  productPageId,
  quantityOfSimilarProducts = 3
) => {
  const productIds = [];

  products.forEach((product, index) => {
    if (
      !productIds.includes(product.id) &&
      product.id !== productPageId &&
      products.length >= index + 1 &&
      productIds.length < quantityOfSimilarProducts
    ) {
      productIds.push(product.id);
    }
  });

  return productIds.map((id) => products.find((product) => product.id === id));
};

module.exports = { selectRandomUniqueSimilarProducts };
