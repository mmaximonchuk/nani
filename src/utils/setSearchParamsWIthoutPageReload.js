const setSearchParamsWIthoutPageReload = (params) => {
  const newRelativePathQuery = window.location.pathname + '?' + params.toString();
  window.history.pushState(null, "", newRelativePathQuery);
};

module.exports = { setSearchParamsWIthoutPageReload };
