const { ROUTES } = require("../constants/routes");

async function createPaymentLink(
  amount,
  ccy = 980,
  redirectUrl = `http://localhost:8000/${ROUTES.THANK_YOU}`,
  merchantPayId,
  token,
  items
) {
  const url = "https://api.monobank.ua/api/merchant/invoice/create";

  const body = {
    amount: amount * 100, // Сумма в копейках
    ccy, // Валюта (980 - UAH)
    redirectUrl, // URL для перенаправления
    merchantPaymInfo: {
      reference: merchantPayId, // Уникальный ID платежа
      destination: "Покупка NANI",
      comment: "Покупка NANI",
      customerEmails: [],
      basketOrder: items.map((item) => ({
        name: item.title,
        qty: item.quantity,
        sum: Number(item.price) * 100,
        icon: item.cardImage,
        unit: "шт.",
        code: "d21da1c47f3c45fca10a10c32518bdeb",
        // discounts: [
        //   {
        //     type: "DISCOUNT",
        //     mode: "PERCENT",
        //     value: 0.01,
        //   },
        // ],
      })),
    },
    webHookUrl:
      "https://example.com/mono/acquiring/webhook/maybesomegibberishuniquestringbutnotnecessarily",
    validity: 3600,
    paymentType: "debit",
  };

  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "X-Token": token, // Ваш API токен
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });

    const result = await response.json();
    console.log("Ссылка на оплату:", result);
    return [result, null];
  } catch (error) {
    console.error("Ошибка создания платежа:", error);
    return [null, error];
  }
}

module.exports = { createPaymentLink };
