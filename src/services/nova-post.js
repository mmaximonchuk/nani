const { filterCities } = require("../utils/filterCities");
const { debounce } = require("../utils/debounce");

const unoptimizedLoadCitiesOptions = async (inputValue, callback) => {
  const apiKey = "afceb0f0a98a6eb75c2390e0997c6795";
  const method = "getCities";
  const apiUrl = "https://api.novaposhta.ua/v2.0/json/";

  const response = await fetch(apiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      apiKey: apiKey,
      modelName: "Address",
      calledMethod: method,
    }),
  });
  //     "apiKey": "",
  //     "modelName": "Address",
  //     "calledMethod": "getCities",
  //     "methodProperties": {
  //     "CityRef": "8d5a980d-391c-11dd-90d9-001a92567626"
  // }

  const result = await response.json();

  callback(
    filterCities(
      inputValue,
      result.data.map((item) => ({
        value: item.Ref,
        label: item.Description,
      }))
    )
  );
};

const loadCitiesOptions = debounce(unoptimizedLoadCitiesOptions, 1000);

module.exports = { loadCitiesOptions };
