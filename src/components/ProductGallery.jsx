import React from "react";
import ImageGallery from "react-image-gallery";
import { useResizeScreen } from "../hooks/useResizeScreen";

import "react-image-gallery/styles/css/image-gallery.css";

// const images = [
//   {
//     original: "https://picsum.photos/id/1018/1000/600/",
//     thumbnail: "https://picsum.photos/id/1018/250/150/",
//     originalWidth: 440,
//   },
//   {
//     original: "https://picsum.photos/id/1015/1000/600/",
//     thumbnail: "https://picsum.photos/id/1015/250/150/",
//     originalWidth: 440,
//   },
//   {
//     original: "https://picsum.photos/id/1019/1000/600/",
//     thumbnail: "https://picsum.photos/id/1019/250/150/",
//     originalWidth: 440,
//   },
// ];

export const ProductGallery = ({ galleryItems = [], startIndex = 0 }) => {
  const { isMobile } = useResizeScreen();
  
  return (
    <div className="gallery col-12 col-md-6">
      <ImageGallery
        lazyLoad={true}
        showBullets={false}
        showNav={false}
        thumbnailPosition={isMobile ? "bottom" : "left"}
        showPlayButton={false}
        startIndex={startIndex}
        showFullscreenButton={false}
        items={galleryItems}
      />
    </div>
  );
};
