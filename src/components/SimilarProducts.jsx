import React from "react";
import * as prismicH from "@prismicio/helpers";

import { ProductCard } from "./ProductCard";

import useAllPrismicProducts from "../hooks/useAllPrismicProducts";
import { selectRandomUniqueSimilarProducts } from "../utils/selectRandomUniqueSimilarProducts";

const SimilarProducts = ({ type, productPageId }) => {
  const { allProducts } = useAllPrismicProducts();

  const filteredProductsByType =
    allProducts?.filter((product) => product.data.type === type) ?? [];
  const similarProducts = selectRandomUniqueSimilarProducts(
    filteredProductsByType,
    productPageId
  );

  return (
    <section className="row ">
      <div className="col-12 ">
        <h2 className="c-title-26 similar-products">Схожі товари</h2>
      </div>
      {!similarProducts?.length && (
        <div className="col-12">
          На жаль, поки що в нас немає схожих товарів
        </div>
      )}
      {similarProducts?.map(
        ({
          data: {
            card_image,
            gallery,
            price: { text: price },
            title: { text: title },
            type,
            is_in_black,
            is_in_white,
          },
          id,
        }) => {
          const cardImage = prismicH.asImageSrc(card_image);

          return (
            <div key={id} className="product-col col-md-4 col-sm-6 col-6">
              <ProductCard
                id={id}
                type={type}
                title={title}
                price={price}
                thumb={cardImage}
                gallery={gallery}
                isInWhite={is_in_white}
                isInBlack={is_in_black}
              />
            </div>
          );
        }
      )}
    </section>
  );
};

export default SimilarProducts;
