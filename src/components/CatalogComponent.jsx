import React, { useEffect, useMemo, useState } from "react";
import * as prismicH from "@prismicio/helpers";

import { ProductCard } from "./ProductCard";
import { GoBackSection } from "./GoBackSection";
import { Button } from "./Button";

import { ANIME_NAMES } from "../constants/product-types";
import { setSearchParamsWIthoutPageReload } from "../utils/setSearchParamsWIthoutPageReload";

import IconCloseMenu from "../assets/images/svg/closeModal.inline.svg";
import iconFilters from "../assets/images/svg/filters.svg";
import { useResizeScreen } from "../hooks/useResizeScreen";

const CatalogComponent = ({ data }) => {
  const {
    onSelectFilter,
    visibleFilters,
    onCloseFilters,
    onOpenFilters,
    filters,
    filteredProducts,
  } = useFilters(data);

  return (
    <div>
      <div className="catalog">
        <GoBackSection />
        <section className="second-section">
          <div className="container">
            <div className="filters">
              <h2 className="c-title-26 filters-title">Футболки</h2>
              <button
                onClick={onOpenFilters}
                type="button"
                className={`filters-btn ${filters.length > 0 ? "active" : ""}`}
              >
                <img src={iconFilters} alt="Фільтри" />
                <span className="filters-btn__accent"> Фільтри</span>
              </button>
              <div
                className={`filters-container ${
                  visibleFilters ? "visible" : ""
                }`}
              >
                <Button
                  onClick={onCloseFilters}
                  variant="text"
                  className="btn-close-filters"
                >
                  <IconCloseMenu className="btn-close-icon" />
                </Button>
                {Object.entries(ANIME_NAMES).map(([key, animeName]) => {
                  const isSelected = filters.includes(key);
                  return (
                    <label className="filter-label">
                      <input
                        className="filter-input visually-hidden"
                        type="checkbox"
                        name="animeName"
                        value={key}
                        checked={isSelected}
                        onChange={() => onSelectFilter(key)}
                      />
                      <span className="filter-fake" />
                      <span className="c-text-18 filter-text">{animeName}</span>
                    </label>
                  );
                })}
              </div>
            </div>
            <div className="catalog-list">
              {filteredProducts?.map(
                ({
                  card_image,
                  gallery,
                  id,
                  // material,
                  price: { text: price },
                  title: { text: title },
                  type,
                  is_in_black,
                  is_in_white,
                }) => {
                  const cardImage = prismicH.asImageSrc(card_image);

                  return (
                    <div key={id} className="grid-col">
                      <ProductCard
                        id={id}
                        type={type}
                        title={title}
                        price={price}
                        thumb={cardImage}
                        gallery={gallery}
                        isInWhite={is_in_white}
                        isInBlack={is_in_black}
                        // colors={colors}
                        // density={density}
                        // material={material}
                      />
                    </div>
                  );
                }
              )}
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

const useFilters = (data) => {
  const { isMobile } = useResizeScreen();
  const [filters, setFilters] = useState([]);
  const [visibleFilters, setVisibleFilters] = useState(isMobile ? false : true);

  useEffect(() => {
    if (!isMobile) setVisibleFilters(true);

    setVisibleFilters(false);
  }, [isMobile]);

  useEffect(() => {
    setFilters(
      Array.from(new URLSearchParams(window.location.search).entries()).map(
        ([_, value]) => value
      )
    );
  }, []);

  const onCloseFilters = () => {
    setVisibleFilters(false);
  };

  const onOpenFilters = () => {
    setVisibleFilters(true);
  };

  const onSelectFilter = (anime) => {
    const params = new URLSearchParams(window.location.search);
    if (filters.some((item) => item === anime)) {
      params.delete("animeName", anime);
      setFilters(
        Array.from(new URLSearchParams(params).entries()).map(
          ([_, value]) => value
        )
      );
      setSearchParamsWIthoutPageReload(params);

      return;
    }
    params.append("animeName", anime);
    setFilters(
      Array.from(new URLSearchParams(params).entries()).map(
        ([_, value]) => value
      )
    );
    setSearchParamsWIthoutPageReload(params);
  };

  const products = data?.map((item) => ({ ...item.data, id: item.id }));
  const filteredProducts = useMemo(
    () =>
      products?.filter((product) => {
        if (!filters.length) return true;
        return filters.some((filter) => product.anime === filter);
      }),
    [products, filters]
  );
  return {
    onSelectFilter,
    filteredProducts,
    filters,
    visibleFilters,
    onCloseFilters,
    onOpenFilters,
  };
};

export default CatalogComponent;
