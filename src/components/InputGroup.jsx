import React, { forwardRef } from "react";
import { DropdownSelect, DropdownSelectAsync } from "./Select";

const withInputVariant = (Component) => {
  const ComponentWithVariant = forwardRef(
    (
      {
        placeholderClassName = "",
        placeholder,
        inputClassName = "",
        lableClassName = "",
        wrapperClassName = "",
        type,
        name,
        htmlType,
        ...restProps
      },
      ref
    ) => {
      switch (type) {
        case "select-async":
          return (
            <Component
              wrapperClassName={wrapperClassName}
              lableClassName={lableClassName}
            >
              <DropdownSelectAsync
                placeholder={placeholder}
                className="input-group__select"
                {...restProps}
              />
            </Component>
          );
        case "select":
          return (
            <Component
              wrapperClassName={wrapperClassName}
              lableClassName={lableClassName}
            >
              <DropdownSelect
                placeholder={placeholder}
                className="input-group__select"
                {...restProps}
              />
            </Component>
          );
        default:
          return (
            <Component
              wrapperClassName={wrapperClassName}
              lableClassName={lableClassName}
            >
              <input
                className={`input-group__input ${inputClassName}`}
                type={htmlType}
                placeholder=" "
                name={name}
                ref={ref}
                {...restProps}
              />
              {placeholder && (
                <span
                  className={`input-group__placeholder ${placeholderClassName}`}
                >
                  {placeholder}
                </span>
              )}
            </Component>
          );
      }
    }
  );
  return ComponentWithVariant;
};

export const InputGroup = withInputVariant(
  ({ lableClassName, children, wrapperClassName }) => {
    return (
      <div className={wrapperClassName}>
        <label className={`input-group__label ${lableClassName}`}>
          {children}
        </label>
      </div>
    );
  }
);
