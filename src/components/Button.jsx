import React from "react";

// const withVariantsButton = (Component) => {
//   const ComponentWithVariant = ({ variant, ...restProps }) => {
//     switch (variant) {
//       case "primary":
//         return <Component variant="primary" {...restProps} />;
//       case "secondary":
//         return <Component variant="secondary" {...restProps} />;
//     }
//     return <Component {...restProps} />;
//   };

//   return ComponentWithVariant;
// };

export const Button = ({
  children,
  className,
  variant,
  type = "button",
  ...restProps
}) => {
  return (
    <button
      className={`c-btn ${variant} ${className}`}
      type={type}
      {...restProps}
    >
      {children}
    </button>
  );
};
