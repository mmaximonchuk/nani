import { Link } from "gatsby";
import React from "react";

import { ROUTES } from "../constants/routes";

import IconArrow from "../assets/images/svg/slider-arrow.inline.svg";

export const GoBackSection = ({ backTo = ROUTES.CATALOG }) => {
  return (
    <div className="first-section">
      <div className="container">
        <Link to={backTo} className="c-text-14 first-section__btn">
          <IconArrow className="first-section__arrow" /> Повернутись
        </Link>
      </div>
    </div>
  );
};
