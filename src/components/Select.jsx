import React from "react";
import Select from "react-select";
import AsyncSelect from "react-select/async";

export const DropdownSelect = ({
  className,
  options,
  placeholder,
  ...restProps
}) => {
  const colourStyles = {
    control: (styles, { isFocused }) => ({
      ...styles,
      backgroundColor: "transparent",
      border: "none",
      boxShadow: isFocused ? "none" : "none",
      borderRadius: 0,
      borderBottom: isFocused ? "1px solid #424b5a" : "1px solid #424b5a",
      ":hover": {
        borderBottom: "1px solid #424b5a",
      },
    }),
    option: (styles, { isSelected }) => ({
      ...styles,
      fontSize: "14px",
      color: isSelected ? "#fff" : "#424b5a",
      backgroundColor: isSelected ? "#424b5a" : "#fff",
      fontFamily: "Klein",
    }),
    input: (styles) => ({ ...styles }),
    placeholder: (styles) => ({
      ...styles,
      fontSize: "14px",
      fontFamily: "Klein",
      color: "#424b5a",
    }),
    singleValue: (styles) => ({
      ...styles,
      fontSize: "14px",
      fontFamily: "Klein",
      ":before": {
        display: "none",
      },
    }),
    dropdownIndicator: (styles) => ({
      ...styles,
      color: "#424b5a",
      ":hover": { color: "#424b5a" },
    }),
    indicatorSeparator: () => ({ dispalay: "none" }),
    valueContainer: (styles) => ({ ...styles, paddingLeft: "13px" }),
  };

  return (
    <Select
      className={className}
      classNamePrefix="select"
      isClearable={true}
      name="color"
      placeholder={placeholder}
      styles={colourStyles}
      options={options}
      {...restProps}
    />
  );
};

export const DropdownSelectAsync = ({
  className,
  placeholder,
  loadOptions,
  ...restProps
}) => {
  const colourStyles = {
    control: (styles, { isFocused }) => ({
      ...styles,
      backgroundColor: "transparent",
      border: "none",
      boxShadow: isFocused ? "none" : "none",
      borderRadius: 0,
      borderBottom: isFocused ? "1px solid #424b5a" : "1px solid #424b5a",
      ":hover": {
        borderBottom: "1px solid #424b5a",
      },
    }),
    option: (styles, { isSelected }) => ({
      ...styles,
      fontSize: "14px",
      color: isSelected ? "#fff" : "#424b5a",
      backgroundColor: isSelected ? "#424b5a" : "#fff",
      fontFamily: "Klein",
    }),
    input: (styles) => ({ ...styles }),
    placeholder: (styles) => ({
      ...styles,
      fontSize: "14px",
      fontFamily: "Klein",
      color: "#0E0E0E",
    }),
    singleValue: (styles) => ({
      ...styles,
      fontSize: "14px",
      fontFamily: "Klein",
      ":before": {
        display: "none",
      },
    }),
    dropdownIndicator: (styles) => ({
      ...styles,
      color: "#424b5a",
      ":hover": { color: "#424b5a" },
    }),
    indicatorSeparator: () => ({ dispalay: "none" }),
    valueContainer: (styles) => ({ ...styles, paddingLeft: "13px" }),
  };

  return (
    <AsyncSelect
      className={className}
      classNamePrefix="select"
      isClearable={true}
      name="color"
      placeholder={placeholder}
      styles={colourStyles}
      // defaultOptions={[{ value: "лалала", label: "Почніть вводити назву міста" }]}
      cacheOptions
      loadOptions={loadOptions}
      {...restProps}
    />
  );
};
