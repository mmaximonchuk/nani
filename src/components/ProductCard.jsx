import React, { useEffect, useState } from "react";
import { Link } from "gatsby";
// import * as prismicH from '@prismicio/helpers';

import { ROUTES } from "../constants/routes";

import imgLogo from "../assets/images/logo.png";
import { asImageSrc } from "@prismicio/helpers";
import { useLocation } from "@reach/router";

export const ProductCard = ({
  id,
  // type,
  title,
  thumb,
  price,
  gallery,
  isInWhite,
  isInBlack,
}) => {
  const [isHovering, setIsHovering] = useState(false);
  const [activeCardImg, setActiveCardImg] = useState(thumb);

  useEffect(() => {
    if (!isHovering || !isInWhite) {
      const timeoutId = setTimeout(() => {
        setActiveCardImg(thumb);
      }, 500);
      return () => clearTimeout(timeoutId);
    }

    const firstWhiteItem = gallery.find((item) => item.color === "white");
    const whiteImageUrl = asImageSrc(firstWhiteItem.gallery_image);
    setActiveCardImg(whiteImageUrl);
  }, [isHovering]);

  const onMouseOver = () => {
    setIsHovering(true);
  };
  const onMouseOut = () => {
    setIsHovering(false);
  };

  return (
    <div
      className="product-card"
      onMouseOver={onMouseOver}
      onMouseOut={onMouseOut}
    >
      <Link to={`${ROUTES.PRODUCTS}/${id}`}>
        <div className="product-card__head">
          <img
            className="product-card__img"
            src={activeCardImg ?? imgLogo}
            alt={title}
          />
        </div>
        <div className="product-card__body">
          <h3 className="c-text-18 product-card__title">{title}</h3>
          <p className="c-text-18 product-card__price">{price} грн</p>
        </div>
      </Link>
    </div>
  );
};
