/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import * as React from "react";
import { Helmet } from "react-helmet";
// import { useStaticQuery, graphql } from "gatsby";

const SEO = ({ description = "", lang, title = "", children }) => {
  const metaDescription = description;
  const defaultTitle = title;

  return (
    <Helmet>
      <title>{defaultTitle ? `${title} | ${defaultTitle}` : title}</title>
      <meta name="description" content={metaDescription} />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={metaDescription} />
      <meta property="og:type" content="website" />
      <meta name="twitter:card" content="summary" />
      <link rel="icon" href="/favicon-32x32.svg" type="image/svg" />
      {/* <meta
        name="twitter:creator"
        content={site?.siteMetadata?.social?.twitter || ``}
      /> */}
      {/* <meta name="twitter:title" content={title} /> */}
      {/* <meta name="twitter:description" content={metaDescription} /> */}
      {/* {children} */}
    </Helmet>
  );
};

export default SEO;
