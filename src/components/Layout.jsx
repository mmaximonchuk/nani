import React, { useEffect, useState } from "react";
import { Link } from "gatsby";

import { ROUTES } from "../constants/routes";

import { SEO } from "../components";

import IconNaniLogo from "../assets/images/svg/logo.inline.svg";
import IconCart from "../assets/images/svg/cart.inline.svg";
import IconBurgerMenu from "../assets/images/svg/burger-menu.inline.svg";
// import IconTelegram from "../assets/images/svg/telegram.inline.svg";
import IconInstargam from "../assets/images/svg/instargram.inline.svg";
import IconArrowRight from "../assets/images/svg/arrow-right.inline.svg";
import IconCloseMenu from "../assets/images/svg/close-menu.inline.svg";

import "../assets/styles/main.scss";
import { useCart } from "react-use-cart";

const Layout = ({ location, title, descritpion, children }) => {
  const { items } = useCart();
  console.log("items: ", items);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [detailedMenuType, setDetailedMenuType] = useState(null); // null | "Про нас" | "Каталог"
  const rootPath = `${__PATH_PREFIX__}/`;
  const isRootPath = location?.pathname === rootPath;

  useEffect(() => {
    if (isMenuOpen) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "auto";
    }
  }, [isMenuOpen]);

  return (
    <div className="layout">
      <SEO descritpion={descritpion} title={title} />
      <header className="header">
        <div className="container">
          <button
            className="burger-menu-btn"
            onClick={() => {
              setIsMenuOpen((prevState) => !prevState);
              if (isMenuOpen) {
                setDetailedMenuType(null);
              }
            }}
          >
            {isMenuOpen ? (
              <IconCloseMenu className="burger-menu-icon icon-close" />
            ) : (
              <IconBurgerMenu className="burger-menu-icon" />
            )}
          </button>
          <Link className="logo-link" to={ROUTES.HOMEPAGE}>
            <IconNaniLogo className="logo" />
          </Link>
          <nav className="header-nav">
            <div className="nav-link">
              <span className="nav-link__trigger text c-text-12">Про нас</span>
              <div className="dropdown-backdrop">
                <ul className="dropdown-menu">
                  <li className="dropdown-menu-item">
                    <Link to={ROUTES.ABOUT} className="dropdown-menu-link">
                      Хто такі Nani?!
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
            <div className="nav-link">
              <Link to={ROUTES.CATALOG} className="nav-link__trigger text c-text-12">
                Каталог
              </Link>
              <div className="dropdown-backdrop">
                <ul className="dropdown-menu">
                  <li className="dropdown-menu-item">
                    <Link
                      to={ROUTES.CATALOG_TSHIRT}
                      className="dropdown-menu-link"
                    >
                      Футболки
                    </Link>
                  </li>
                  <li className="dropdown-menu-item">
                    <Link
                      to={ROUTES.CATALOG_HOODIE}
                      className="dropdown-menu-link"
                    >
                      Худі
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
            <Link className="nav-link" to={ROUTES.DELIVERY_AND_PAYMENT}>
              <span className="text c-text-12">Умови доставки та оплати</span>
            </Link>
            <Link className="cart-wrapper" to={ROUTES.CART}>
              <IconCart className="icon" />
              {items.length > 0 && (
                <span className="cart-quantity">{items.length}</span>
              )}
              <span className="text c-text-24">Кошик</span>
            </Link>
          </nav>
          <Link
            aria-label="Кошик"
            className="cart-wrapper mobile"
            to={ROUTES.CART}
          >
            <IconCart className="icon" />
            {items.length > 0 && (
              <span className="cart-quantity">{items.length}</span>
            )}
          </Link>
        </div>
        <div className={`mobile-menu ${isMenuOpen ? "visible" : ""}`}>
          <ul className="mobile-menu-list">
            <li className="mobile-menu-item">
              <button
                onClick={() => setDetailedMenuType("Про нас")}
                className="mobile-menu-item-trigger"
              >
                Про нас <IconArrowRight className="mobile-menu-arrow" />
              </button>
            </li>
            <li className="mobile-menu-item">
              <button
                onClick={() => setDetailedMenuType("Каталог")}
                className="mobile-menu-item-trigger"
              >
                Каталог <IconArrowRight className="mobile-menu-arrow" />
              </button>
            </li>
            <li className="mobile-menu-item">
              <Link className="mobile-menu-item-trigger" to={ROUTES.ORDER}>
                Умови доставки та оплати
              </Link>
            </li>
          </ul>
          <div
            className={`menu-lvl-1 ${
              detailedMenuType !== null ? "visible" : ""
            }`}
          >
            <button
              onClick={() => setDetailedMenuType(null)}
              className="menu-lvl-1-back-btn"
            >
              <IconArrowRight className="selected-mobile-menu-arrow" />{" "}
              {detailedMenuType}
            </button>
            <ul className="selected-mobile-menu">
              {detailedMenuType === "Про нас" && (
                <li className="selected-mobile-menu-item">
                  <Link to={ROUTES.CART} className="selected-mobile-menu-link">
                    Хто такі Nani?!
                  </Link>
                </li>
              )}
              {detailedMenuType === "Каталог" && (
                <>
                  <li className="selected-mobile-menu-item">
                    <Link
                      to={ROUTES.CATALOG}
                      className="selected-mobile-menu-link"
                    >
                      Загальний каталог
                    </Link>
                  </li>
                  <li className="selected-mobile-menu-item">
                    <Link
                      to={ROUTES.CATALOG_TSHIRT}
                      className="selected-mobile-menu-link"
                    >
                      Футболки
                    </Link>
                  </li>
                  <li className="selected-mobile-menu-item">
                    <Link
                      to={ROUTES.CATALOG_HOODIE}
                      className="selected-mobile-menu-link"
                    >
                      Худі
                    </Link>
                  </li>
                </>
              )}
            </ul>
          </div>
        </div>
      </header>
      <main>{children}</main>
      <footer className="footer">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-4 col-sm-6  col-12">
              <Link
                aria-label="NaniUA Logo"
                className="logo-link"
                to={ROUTES.HOMEPAGE}
              >
                <IconNaniLogo className="logo" />
                {/* <img className="logo" src={imgNaniLogo} alt="Nani" /> */}
              </Link>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6  col-12 d-flex flex-column">
              <h3 className="chapter-title c-text-18">Про нас</h3>
              <Link to={ROUTES.ABOUT} className="chapter-link">
                Хто такі NANI?!
              </Link>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6  col-12 d-flex flex-column">
              <h3 className="chapter-title c-text-18">Клієнтам</h3>
              <Link to="#" className="c-text-16 chapter-link">
                Умови доставки та оплати
              </Link>
              <Link to="#" className="c-text-16 chapter-link">
                Повернення та обмін
              </Link>
              <Link to="#" className="c-text-16 chapter-link">
                Політика конфіденційності
              </Link>
              <Link to="#" className="c-text-16 chapter-link">
                Публічна оферта
              </Link>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6  col-12 d-flex flex-column">
              <h3 className="c-text-16 chapter-title">Контакти</h3>
              <p className="c-text-16 chapter-uptext">З питань співпраці:</p>
              <a
                href="mailto:naniukraine@gmail.com"
                className="c-text-16 chapter-link"
              >
                naniukraine@gmail.com
              </a>
              <a className="c-text-18 insta-link" href="#">
                <IconInstargam className="inst-icon" />
                naniukraine
              </a>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <p className="c-text-16">
                Всі права захищені | {new Date().getFullYear()}
              </p>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default Layout;
