import React, { useEffect } from "react";
import IconCloseMenu from "../assets/images/svg/closeModal.inline.svg";
import { Button } from "./Button";

export const Modal = ({ onClose, isModalOpen, children }) => {
  useEffect(() => {
    const handleEscapeClick = (e) => e.code === "Escape" && onClose();

    window.addEventListener("keydown", handleEscapeClick);

    return () => window.removeEventListener("keydown", handleEscapeClick);
  }, [onClose]);

  const handleBackDropClick = (e) => e.target === e.currentTarget && onClose();

  return (
    <div
      className={["backdrop", isModalOpen ? "visible" : ""].join(" ")}
      onClick={handleBackDropClick}
    >
      <div className="modal">
        <Button onClick={onClose} variant="text" className="btn-close-modal">
          <IconCloseMenu className="btn-close-icon" />
        </Button>
        {children}
      </div>
    </div>
  );
};
