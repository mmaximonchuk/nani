import React, { useState } from "react";

import { Button } from "./Button";
import { Modal } from "./Modal";

import IconCart from "../assets/images/svg/cart.inline.svg";
import imgHoodiSizes from "../assets/images/product-details/Thudie.svg";
import imgTshirtSizes from "../assets/images/product-details/Tshirt.svg";
import { PRODUCT_COLORS, PRODUCT_SIZES } from "../constants/product-types";

import { useCart } from "react-use-cart";
import {
  HOODIE_PRODUCT_TYPE,
  TSHIRT_PRODUCT_TYPE,
} from "../constants/categoryTypeToName";

const sizesTableByType = {
  [HOODIE_PRODUCT_TYPE]: imgHoodiSizes,
  [TSHIRT_PRODUCT_TYPE]: imgTshirtSizes,
};

export const ProductDetails = ({
  id,
  type,
  title: { text: title },
  price: { text: price },
  material: { text: material },
  density: { text: density },
  isInWhite,
  isInBlack,
  onSelectPreferredColor,
  selectedPreferredColor,
  selectedPreferredSize,
  onSelectPreferredSize,
  onAddItem,
  onBuyInOneClick,
}) => {
  const { inCart } = useCart();

  const [isModalOpen, setIsModalOpen] = useState(false);

  const closeModal = () => setIsModalOpen(false);
  const openModal = () => setIsModalOpen(true);

  const isProductInCart = inCart(id);
  const sizeImg = sizesTableByType[type];
  return (
    <div className="details col-12 col-md-6">
      <div className="details-block">
        <h1 className="details-title c-title-26">
          {title}{" "}
          <Button variant="secondary" className="add-to-cart-btn">
            <IconCart className="icon-add-to-cart" />
          </Button>
        </h1>
        <p className="c-text-20 price">Ціна: {price}грн</p>
      </div>
      <div className="details-block">
        <h2 className="details-size-title c-text-18">Оберіть розмір:</h2>
        <div className="sizes">
          {PRODUCT_SIZES.map((size) => (
            <label key={size} className="size-label">
              <input
                className="size-input visually-hidden"
                type="radio"
                name="size"
                value={size}
                checked={selectedPreferredSize === size}
                onChange={() => onSelectPreferredSize(size)}
              />
              <div className="size-fake c-text-16">{size.toUpperCase()}</div>
            </label>
          ))}
        </div>
        <button type="button" onClick={openModal} className="size-table-btn">
          Таблиця розмірів
        </button>
      </div>
      <div className="details-block">
        <h2 className="details-size-title c-text-18">Оберіть колір:</h2>
        <div className="colors">
          {PRODUCT_COLORS.filter((color) => {
            if (isInBlack && isInWhite) {
              return color === "black" || color === "white";
            }
            if (isInBlack && !isInWhite) return color === "black";
            return color === "white";
          }).map((color) => (
            <label key={color} className="colors-label">
              <input
                className="colors-input visually-hidden"
                type="radio"
                name="color"
                value={color}
                checked={color === selectedPreferredColor}
                onChange={() => onSelectPreferredColor(color)}
              />
              <div style={{ backgroundColor: color }} className="colors-fake" />
            </label>
          ))}
        </div>
        <p className="details-description c-text-16">
          Матеріал: {material ?? "не вказано"}
        </p>
        <p className="details-description c-text-16">
          Щільність: {density ?? "не вказано"} г
        </p>
      </div>
      <div className="details-block">
        {/* <p className="c-text-20 description">{description}</p> */}
      </div>
      <div className="details-btns">
        <Button
          // disabled={isProductInCart}
          onClick={onAddItem}
          variant="secondary"
        >
          Додати в кошик
        </Button>
        {/* <Link to={ROUTES.ORDER}> */}
        <Button onClick={onBuyInOneClick} variant="primary">
          купити в один клік
        </Button>
        {/* </Link> */}
      </div>
      <Modal isModalOpen={isModalOpen} onClose={closeModal}>
        <img className="modal-img" src={sizeImg} alt={title} />
      </Modal>
    </div>
  );
};
