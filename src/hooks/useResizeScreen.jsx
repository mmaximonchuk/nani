import { useState, useEffect } from "react";

const IS_MOBILE = () =>
  typeof window !== "undefined" ? window.innerWidth <= 768 - 0.02 : 0;
const IS_TABLET = () =>
  typeof window !== "undefined" ? window.innerWidth <= 992 - 0.02 : 0;

export const useResizeScreen = () => {
  const [isMobile, setMobile] = useState(false);
  const [isTablet, setTablet] = useState(false);

  const handleResize = () => {
    setMobile(IS_MOBILE());
    setTablet(IS_TABLET());
  };

  useEffect(() => {
    setMobile(IS_MOBILE());
    setTablet(IS_TABLET());
  }, []);

  useEffect(() => {
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  });

  return { isMobile, isTablet };
};

