import { graphql, useStaticQuery } from 'gatsby';

const useAllPrismicProducts = () => {
    const {
        allPrismicProduct: { nodes: allProducts },
      } = useStaticQuery(graphql`
        query {
          allPrismicProduct {
            nodes {
              data {
                card_image {
                  url
                }
                density {
                  text
                }
                gallery {
                  gallery_image {
                    url
                  }
                  color
                }
                material {
                  text
                }
                price {
                  text
                }
                title {
                  text
                }
                type
                is_in_black
                is_in_white
                anime
              }
              id
            }
          }
        }
      `);


  return {allProducts};
}

export default useAllPrismicProducts;
