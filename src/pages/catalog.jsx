import React from "react";
import { Link } from "gatsby";

import { Layout } from "../components";

import { ROUTES } from "../constants/routes";

import imgCatalogFut from "../assets/images/homepage/fut.jpg";
import imgCatalogHudi from "../assets/images/homepage/hudi.png";

import "../assets/styles/pages/home.scss";

function CatalogPage() {
  return (
    <Layout title="Нові колекції">
      <div className="home-page">
        <section className="second-section">
          <div className="container">
            <h2 className="visually-hidden">Нані Каталог</h2>
            <ul className="catalog">
              <li className="catalog__item">
                <Link to={ROUTES.CATALOG_TSHIRT} className="catalog__link">
                  <h3 className="c-title-36 catalot__item-name">Футболки</h3>
                  <div className="catalog__img-wrapper">
                    <img
                      className="catalog__img"
                      src={imgCatalogFut}
                      alt="Нані Футболки"
                    />
                  </div>
                </Link>
              </li>
              <li className="catalog__item">
                <Link to={ROUTES.CATALOG_HOODIE} className="catalog__link">
                  <h3 className="c-title-36 catalot__item-name">Худі</h3>
                  <div className="catalog__img-wrapper">
                    <img
                      className="catalog__img"
                      src={imgCatalogHudi}
                      alt="Нані Худі"
                    />
                  </div>
                </Link>
              </li>
            </ul>
          </div>
        </section>
      </div>
    </Layout>
  );
}

export default CatalogPage;
