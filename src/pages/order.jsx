import React, { useEffect, useState } from "react";
import { Link, navigate } from "gatsby";
import { useForm } from "react-hook-form";
import { useCart } from "react-use-cart";
// import { countries } from "countries-list";
import { Button, InputGroup, Layout, Loader } from "../components";

import { ROUTES } from "../constants/routes";

import IconArrow from "../assets/images/svg/slider-arrow.inline.svg";

import "../assets/styles/pages/order.scss";
import { loadCitiesOptions } from "../services/nova-post";
import toast from "react-hot-toast";
import { filterCities } from "../utils/filterCities";
import { createPaymentLink } from "../services/monobank";

const orderLocationOptions = {
  "delivery-in-ukraine": "Доставка по Україні",
  "delivery-out-of-ukraine": "Доставка за кордон",
};
const defaultDeliveryTypes = {
  courier: "Кур'єр",
  novaposhta: "Нова пошта",
};

const options = [
  {
    value: defaultDeliveryTypes["courier"],
    label: defaultDeliveryTypes["courier"],
  },
  {
    value: defaultDeliveryTypes["novaposhta"],
    label: defaultDeliveryTypes["novaposhta"],
  },
];



const Order = () => {
  const { items } = useCart();
  const [orderLocation, setOrderLocation] = useState(
    orderLocationOptions["delivery-in-ukraine"]
  );
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();
  const [deliveringCity, setDeliveringCitiy] = useState(null);
  const [deliveringWarehouse, setDeliveringWarehouse] = useState(null);
  const [deliveryType, setDeliveryType] = useState(null); // Кур'єр, Нова пошта
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isError, setIsError] = useState(false);

  const loadWarehouseOptions = async (inputValue, callback) => {
    const apiKey = "afceb0f0a98a6eb75c2390e0997c6795";
    const method = "getWarehouses";
    const apiUrl = "https://api.novaposhta.ua/v2.0/json/";

    const response = await fetch(apiUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        apiKey: apiKey,
        modelName: "Address",
        calledMethod: method,
        methodProperties: {
          CityRef:
            deliveringCity.value ?? "8d5a980d-391c-11dd-90d9-001a92567626",
        },
      }),
    });

    const result = await response.json();

    callback(
      filterCities(
        inputValue,
        result.data.map((item) => ({
          value: item.Description,
          label: item.Description,
        }))
      )
    );
  };

  const subTotalPrice = items.reduce((acc, item) => {
    return acc + item.itemTotal;
  }, 0);

  const totalPrice = subTotalPrice;

  useEffect(() => {
    if (isSuccess) {
      const timeoutId = setTimeout(() => {
        navigate(ROUTES.THANK_YOU);
      }, 2000);

      return () => clearTimeout(timeoutId);
    }
  }, [isSuccess]);

  const onSubmit = async (values) => {
    let finalFormData;
    if (orderLocation === orderLocationOptions["delivery-in-ukraine"]) {
      finalFormData = {
        ...values,
        orderLocation,
        deliveringCity: deliveringCity.label,
        deliveringWarehouse:
          deliveryType.label === defaultDeliveryTypes["courier"]
            ? "--- НЕ ВКАЗАНО ---"
            : deliveringWarehouse.value,
        deliveryType: deliveryType.label,
        subTotalPrice,
        totalPrice,
        userAddress:
          deliveryType.label === defaultDeliveryTypes["novaposhta"]
            ? "--- НЕ ВКАЗАНО ---"
            : values.userAddress,
        items: items.map((item) => {
          return {
            Кількість: item.quantity,
            "Назва товару": item.title,
            Ціна: item.price,
            Колір: item.selectedPreferredColor,
            Розмір: item.selectedPreferredSize,
          };
        }),
      };
    } else {
      finalFormData = {
        ...values,
        orderLocation,
        deliveringCity: "--- НЕ ВКАЗАНО ---",
        deliveringWarehouse: "--- НЕ ВКАЗАНО ---",
        deliveryType: "--- НЕ ВКАЗАНО ---",
        userAddress: "--- НЕ ВКАЗАНО ---",
        subTotalPrice,
        totalPrice,
        items: items.map((item) => {
          return {
            Кількість: item.quantity,
            "Назва товару": item.title,
            Ціна: item.price,
            Колір: item.selectedPreferredColor,
            Розмір: item.selectedPreferredSize,
          };
        }),
      };
    }

    try {
      setIsError(false);
      setIsLoading(true);

      const [response, error] = await createPaymentLink(
        subTotalPrice,
        980,
        `http://localhost:8000${ROUTES.THANK_YOU}`,
        "1234567890",
        process.env.GATSBY_API_MONOBANK_API_TOKEN,
        items
      );
      if(error) {
        throw new Error(error);
      }
      localStorage.setItem("invoiceId", response.invoiceId);
      window.open(response.pageUrl);

      // await fetch(
      //   `https://script.google.com/macros/s/AKfycbwQy3rAbEgLdPRyoR-066pm_GbW7IRkGqq7WQmYblBoIprGoH8z1a3szTY7eTc0V5c/exec?` +
      //     new URLSearchParams({
      //       userName: finalFormData.userName,
      //       userSurname: finalFormData.userSurname,
      //       userEmail: finalFormData.userEmail,
      //       userPhone: finalFormData.userPhone,
      //       userAddress: finalFormData.userAddress,
      //       orderLocation: finalFormData.orderLocation,
      //       deliveringCity: finalFormData.deliveringCity,
      //       deliveringWarehouse: finalFormData.deliveringWarehouse,
      //       deliveryType: finalFormData.deliveryType,
      //       subTotalPrice: finalFormData.subTotalPrice,
      //       totalPrice: finalFormData.totalPrice,
      //       items: JSON.stringify(finalFormData.items),
      //     }),
      //   {
      //     method: "POST",
      //     mode: "no-cors",
      //   }
      // );

      // setIsSuccess(true);
    } catch (err) {
      setIsSuccess(false);
      setIsError(true);
      toast(err.message);
    } finally {
      setIsLoading(false);
      // setShowPopup(true);
      reset();
      setDeliveringCitiy(null);
      setDeliveringWarehouse(null);
      setDeliveryType(null);
    }
  };

  const variantUkraineDelivery =
    orderLocation === orderLocationOptions["delivery-in-ukraine"]
      ? "secondary"
      : "primary";
  const variantOutOfUkraineDelivery =
    orderLocation === orderLocationOptions["delivery-out-of-ukraine"]
      ? "secondary"
      : "primary";
  return (
    <Layout title="Оформити замовлення">
      <div className="order-page">
        <section className="first-section">
          <div className="container">
            <Link to={ROUTES.CART} className="c-text-14 first-section__btn">
              <IconArrow className="first-section__arrow" /> Назад до кошику
            </Link>
          </div>
        </section>
        <div className="container">
          <div className="row heading">
            <div className="col">
              <h1 className="c-title-48">Оформити замовлення</h1>
            </div>
          </div>
          <div className="row">
            <div className="col-md-7 col-12">
              <div className="block-l">
                <h2 className="c-title-32">Введіть свої дані:</h2>
                <div className="d-flex mb-4">
                  <Button
                    onClick={() =>
                      setOrderLocation(
                        orderLocationOptions["delivery-in-ukraine"]
                      )
                    }
                    className="order-location-btn"
                    variant={variantUkraineDelivery}
                    type="button"
                    disabled={isLoading}
                  >
                    {orderLocationOptions["delivery-in-ukraine"]}
                  </Button>
                  <Button
                    onClick={() =>
                      setOrderLocation(
                        orderLocationOptions["delivery-out-of-ukraine"]
                      )
                    }
                    className="order-location-btn"
                    variant={variantOutOfUkraineDelivery}
                    type="button"
                    disabled={isLoading}
                  >
                    {orderLocationOptions["delivery-out-of-ukraine"]}
                  </Button>
                </div>

                <form onSubmit={handleSubmit(onSubmit)} className="order-form">
                  <InputGroup
                    htmlType="text"
                    placeholder="Ім'я"
                    {...register("userName", { minLength: 3, required: true })}
                  />
                  {errors.userName && (
                    <p className="error-msg">Поле "Ім'я" обов'язкове</p>
                  )}
                  <InputGroup
                    htmlType="text"
                    placeholder="Прізвище"
                    {...register("userSurname", {
                      minLength: 3,
                      required: true,
                    })}
                  />
                  {errors.userSurname && (
                    <p className="error-msg">Поле "Прізвище" обов'язкове</p>
                  )}
                  <InputGroup
                    htmlType="text"
                    placeholder="Телефон"
                    {...register("userPhone", { minLength: 7, required: true })}
                  />
                  {errors.userPhone && (
                    <p className="error-msg">Поле "Телефон" обов'язкове</p>
                  )}
                  <InputGroup
                    htmlType="email"
                    placeholder="Ел. пошта"
                    {...register("userEmail", { minLength: 7, required: true })}
                  />
                  {errors.userEmail && (
                    <p className="error-msg"> Поле "Ел. пошта" обов'язкове</p>
                  )}
                  {orderLocation ===
                    orderLocationOptions["delivery-in-ukraine"] && (
                    <>
                      <InputGroup
                        name=""
                        type="select"
                        options={options}
                        onChange={(data) => setDeliveryType(data)}
                        value={deliveryType}
                        placeholder="Спосіб доставки"
                        required
                      />
                      <InputGroup
                        type="select-async"
                        placeholder="Місто"
                        onChange={(data) => {
                          setDeliveringCitiy(data);
                        }}
                        defaultOptions={[
                          {
                            value: "Почніть вводити назву міста",
                            label: "Почніть вводити назву міста",
                          },
                        ]}
                        loadOptions={loadCitiesOptions}
                        required
                        value={deliveringCity}
                      />
                      {deliveryType?.value ===
                        defaultDeliveryTypes["courier"] && (
                        <>
                          <InputGroup
                            name="useAddress"
                            htmlType="text"
                            required
                            placeholder="Вулиця та будинок"
                            {...register("userAddress", { required: true })}
                          />
                          {errors.userAddress && (
                            <p className="error-msg">
                              Поле "Вулиця та будинок" обов'язкове
                            </p>
                          )}
                        </>
                      )}
                      {deliveryType?.value ===
                        defaultDeliveryTypes["novaposhta"] && (
                        <InputGroup
                          value={deliveringWarehouse}
                          type="select-async"
                          loadOptions={loadWarehouseOptions}
                          onChange={(data) => {
                            setDeliveringWarehouse(data);
                          }}
                          required
                          placeholder="№ відділення"
                          defaultOptions={[
                            {
                              value:
                                "Почніть вводити назву вулиці або номер відділення",
                              label:
                                "Почніть вводити назву вулиці або номер відділення",
                            },
                          ]}
                        />
                      )}
                    </>
                  )}
                  <Button
                    type="submit"
                    className="order-form__btn"
                    variant="secondary"
                    disabled={isLoading}
                  >
                    {isLoading ? <Loader /> : "Купити в один клік"}
                  </Button>
                  {isSuccess && (
                    <p className="order-form__success">
                      Замовлення успішно сформовано, ми звяжемося з вами в
                      найближчий час
                    </p>
                  )}
                  {isError && (
                    <p className="order-form__error">
                      Нажаль сталась помилка, спробуйте, будь ласка, пізніше
                    </p>
                  )}
                </form>
              </div>
            </div>
            <div className="col-md-5 col-12">
              {/* <div className="block-r cart">
                <CartItem />
                <CartItem />
              </div> */}
              <div className="cart-summary">
                <p className="cart-summary__text c-text-16">
                  Проміжний підсумок: <b>{subTotalPrice} грн</b>
                </p>
                <p className="cart-summary__text c-text-16">
                  Заощаджено: <b>0 грн</b>
                </p>
                <p className="cart-summary__text c-text-16">
                  Сума до сплати: <b>{totalPrice} грн</b>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

// const CartItem = ({
//   title,
//   thumb,
//   quantity,
//   price,
//   size,
//   color,
//   handleAddItem,
//   handleRemoveItem,
// }) => {
//   return (
//     <div className="cart-item">
//       <div className="cart-item__thumb">
//         <img src={imgCartItem} alt="product title" />
//       </div>
//       <div className="cart-item__body">
//         <h3 className="cart-item__title">Якась назва речі</h3>
//         <p className="c-text-16 cart-item__size">Розмір S, білий</p>
//         <div className="cart-item__controls">
//           <p className="cart-item__price">1000грн</p>

//           <div className="cart-item__wrapper">
//             <button className="cart-item__button cart-item__button--minus">
//               <IconMinus />
//             </button>
//             <span className="c-title-24 cart-item__quantity">1</span>
//             <button className="cart-item__button cart-item__button--plus">
//               <IconPlus />
//             </button>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };

export default Order;
