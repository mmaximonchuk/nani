import React from "react";
import { Link } from "gatsby";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper/modules";
import Carousel from "react-multi-carousel";

import { Layout } from "../components";

// import { useResizeScreen } from "../hooks/useResizeScreen";

import { ROUTES } from "../constants/routes";
import { mockProducts } from "../constants/mocks";

import imgSliderImage from "../assets/images/slider-img.png";
// import imgCatalogFut from "../assets/images/homepage/fut.jpg";
// import imgCatalogHudi from "../assets/images/homepage/hudi.png";

import "react-multi-carousel/lib/styles.css";
import "../assets/styles/pages/home.scss";
function HomePage() {
  const slidesPerView = 1;
  return (
    <Layout title="Нові колекції">
      <div className="home-page">
        <section className="first-section">
          <div className="container">
            <div className="slider">
              <div className="first-section__titles">
                <h2 className="c-title-54">New Drop — 01.07.2023</h2>
                <h1 className="c-title-96">Naruto Style: SS’23</h1>
              </div>
              <div className="old-collections-wrapper">
                <Link
                  to={ROUTES.OLD_COLLECTIONS}
                  className="c-text-18 old-collections__btn"
                >
                  Переглянути старі колекції
                </Link>
              </div>
              <Swiper
                modules={[Pagination]}
                pagination={{
                  clickable: true,
                  renderBullet: function (_, className) {
                    return `<span class="${className}"></span>`;
                  },
                }}
                spaceBetween={24}
                slidesPerView={slidesPerView}
                initialSlide={1}
                centeredSlides
                coverflowEffect={{
                  rotate: 50,
                  stretch: 0,
                  depth: 100,
                  modifier: 1,
                  slideShadows: false,
                }}
              >
                {mockProducts.map(
                  ({
                    id,
                    type,
                    title,
                    thumbs,
                    price,
                    sizes,
                    colors,
                    description,
                  }) => {
                    return (
                      <SwiperSlide key={id}>
                        <img
                          className="slider-image"
                          src={imgSliderImage}
                          alt="Нані Дроп"
                        />
                      </SwiperSlide>
                    );
                  }
                )}
              </Swiper>
            </div>
          </div>
        </section>
        <section className="second-section">
          <div className="container">
            <h2 className="visually-hidden">Нані Каталог</h2>
            <Carousel
              additionalTransfrom={0}
              arrows
              autoPlay
              autoPlaySpeed={3000}
              centerMode={false}
              className=""
              containerClass="container-with-dots"
              dotListClass=""
              draggable
              focusOnSelect={false}
              infinite
              itemClass=""
              keyBoardControl
              minimumTouchDrag={80}
              ssr
             
              pauseOnHover
              renderArrowsWhenDisabled={false}
              renderButtonGroupOutside={false}
              renderDotsOutside={false}
              responsive={{
                desktop: {
                  breakpoint: {
                    max: 3000,
                    min: 1024,
                  },
                  items: 3,
                  partialVisibilityGutter: 40,
                },
                mobile: {
                  breakpoint: {
                    max: 464,
                    min: 0,
                  },
                  items: 1,
                  partialVisibilityGutter: 30,
                },
                tablet: {
                  breakpoint: {
                    max: 1024,
                    min: 464,
                  },
                  items: 2,
                  partialVisibilityGutter: 30,
                },
              }}
              rewind={false}
              rewindWithAnimation={false}
              rtl={false}
              shouldResetAutoplay
              showDots={false}
              sliderClass=""
              slidesToSlide={2}
              swipeable
            >
              <img
                src="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                alt=""
              />
              <img
                src="https://images.unsplash.com/photo-1549396535-c11d5c55b9df?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60"
                alt=""
              />
              <img
                src="https://images.unsplash.com/photo-1550133730-695473e544be?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                alt=""
              />
            </Carousel>
            {/* <ul className="catalog">
              <li className="catalog__item">
                <Link to={ROUTES.CATALOG_TSHIRT} className="catalog__link">
                  <h3 className="c-title-36 catalot__item-name">Футболки</h3>
                  <div className="catalog__img-wrapper">
                    <img
                      className="catalog__img"
                      src={imgCatalogFut}
                      alt="Нані Футболки"
                    />
                  </div>
                </Link>
              </li>
              <li className="catalog__item">
                <Link to={ROUTES.CATALOG_HOODIE} className="catalog__link">
                  <h3 className="c-title-36 catalot__item-name">Худі</h3>
                  <div className="catalog__img-wrapper">
                    <img
                      className="catalog__img"
                      src={imgCatalogHudi}
                      alt="Нані Худі"
                    />
                  </div>
                </Link>
              </li>
            </ul> */}
          </div>
        </section>
      </div>
    </Layout>
  );
}

export default HomePage;
