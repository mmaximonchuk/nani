import React from "react";
import { Link } from "gatsby";

import { Button, Layout } from "../components";
import { ROUTES } from "../constants/routes";

import IconArrow from "../assets/images/svg/slider-arrow.inline.svg";
import imgHorizontalRect1 from "../assets/images/firstRect.png";
import imgVerticalRect1 from "../assets/images/verticalRect.png";

import "../assets/styles/pages/about.scss";

const About = () => {
  return (
    <Layout>
      <div className="about-page">
        <section className="first-section">
          <div className="container">
            <Link to={ROUTES.HOMEPAGE} className="c-text-14 first-section__btn">
              <IconArrow className="first-section__arrow" /> Повернутись
            </Link>
          </div>
        </section>
        <section className="second-section">
          <div className="container">
            <div className="text-block">
              <h1 className="c-title-26 main-title">Про нас:</h1>
              <p className="c-text-18 mb-24">
                Привіт, ми – український аніме-бренд стрітвіру NANI?!,
                заснований молодими амбіціозними підприємцями Анастасією та
                Гасамом.
              </p>
              <p className="c-text-18 mb-36">
                Ми любимо аніме і вирішили пов’язати своє життя з цією культурою
                та популяризувати її через одяг. Нашою метою є створення
                якісного товару з персонажами ваших улюблених тайтлів.
              </p>
            </div>
            <div className="image-rect-wrapper mb-36">
              <img
                src={imgHorizontalRect1}
                alt="Nastya and Gasam"
                width={1080}
                className="image-rect-horizontal"
              />
            </div>
            <div className="text-block">
              <p className="c-text-18 mb-24">
                На наших речах ви не знайдете дизайни з Пінтересту або інших
                брендів, бо ми їх створюємо з 0 із нашими художниками. В основу
                кожної ілюстрації закладені багатогодинні пошуки референсів та
                їхня адаптація під характер кожного з героїв.{" "}
              </p>
              <p className="c-text-18 mb-36">
                Саме тому ми маємо ком’юніті свідомих людей, які підтримують
                українських художників та бізнес і обирають унікальні
                аніме-дизайни від NANI?!
              </p>
            </div>
            <div className="image-rect-wrapper col-3 mb-36">
              <img
                src={imgVerticalRect1}
                alt="Nani design 1"
                width={307}
                className="image-rect-horizontal"
              />
              <img
                src={imgVerticalRect1}
                alt="Marina is drawing on the table"
                width={307}
                className="image-rect-horizontal"
              />
              <img
                src={imgVerticalRect1}
                alt="Nani design 2"
                width={307}
                className="image-rect-horizontal"
              />
            </div>
            <div className="text-block">
              <h2 className="c-title-26 mb-36">Історія:</h2>
              <p className="c-text-18 mb-36">
                У 2022 році ми вирішили ризикнути та створили NANI?! Ми починали
                з невеликої кількості наших перших футболок з дизайном Ітачі
                Учіхи, пошив яких замовляли у підрядників.
              </p>
            </div>
            <div className="image-rect-wrapper col-3 mb-36">
              <img
                src={imgVerticalRect1}
                alt="Nani design 1"
                width={307}
                className="image-rect-horizontal"
              />
              <img
                src={imgVerticalRect1}
                alt="Marina is drawing on the table"
                width={307}
                className="image-rect-horizontal"
              />
              <img
                src={imgVerticalRect1}
                alt="Nani design 2"
                width={307}
                className="image-rect-horizontal"
              />
            </div>

            <div className="text-block mb-36">
              <p className="c-text-18 mb-24">
                Згодом ми вирішили створити невеликий домашній цех, в якому був
                лише один термопрес для перенесення друку дизайнів. Це вже був
                великий крок вперед для нас, оскільки ми оптимізували нашу
                роботу та зменшили строки доставки замовлень для наших клієнтів.
              </p>
            </div>
            <div className="image-rect-wrapper mb-36">
              <img
                src={imgHorizontalRect1}
                alt="----------"
                width={1080}
                className="image-rect-horizontal"
              />
            </div>
            <div className="text-block mb-36">
              <p className="c-text-18 mb-24">
                Кількість замовлень почала зростати, в нас з’явилися постійні
                клієнти, саме тому було прийняте рішення про розширення та
                створення власного виробництва повного циклу. Тепер ми
                контролюємо забезпечення високої якості нашого товару на кожному
                етапі: від вибору тканини, процесу пошиття та до пакування ваших
                замовлень. Ми гарантуємо, що ви будете задоволені якістю нашого
                одягу навіть після 30-го прання.
              </p>
            </div>
            <div className="image-rect-wrapper mb-36">
              <img
                src={imgHorizontalRect1}
                alt="----------"
                width={1080}
                className="image-rect-horizontal"
              />
            </div>
          </div>
        </section>
      </div>
    </Layout>
  );
};

export default About;
