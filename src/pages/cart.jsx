import React from "react";
import { Link } from "gatsby";
import { useCart } from "react-use-cart";

import { Button, Layout } from "../components";

import { ROUTES } from "../constants/routes";
import { PRODUCT_COLORS_TO_NAMES } from "../constants/product-types";

import IconArrow from "../assets/images/svg/slider-arrow.inline.svg";
import IconMinus from "../assets/images/svg/minus.inline.svg";
import IconPlus from "../assets/images/svg/plus.inline.svg";
import IconDelete from "../assets/images/svg/close-menu.inline.svg";
import imgCartItem from "../assets/images/homepage/fut.jpg";

import "../assets/styles/pages/cart.scss";

const Cart = () => {
  const { items, isEmpty } = useCart();

  return (
    <Layout title="Оформити замовлення">
      <div className="cart-page">
        <section className="first-section">
          <div className="container">
            <Link to={ROUTES.CATALOG} className="c-text-14 first-section__btn">
              <IconArrow className="first-section__arrow" /> Повернутись
            </Link>
          </div>
        </section>
        <div className="container">
          <div className="row heading">
            <div className="col">
              <h1 className="c-title-25">Кошик:</h1>
            </div>
          </div>
          <div className="cart">
            {isEmpty && (
              <p className="c-title-25">
                Ваш кошик порожній, щоб вибрати товар, перейдіть в{" "}
                <Link className="c-link" to={ROUTES.CATALOG}>
                  каталог
                </Link>
                .
              </p>
            )}
            {items.map((item) => (
              <CartItem key={item.id} item={item} />
            ))}
          </div>
          <div className="row bottom">
            {!isEmpty && (
              <Button className="create-order__btn" variant="secondary">
                <Link to={ROUTES.ORDER}>Оформити замовлення</Link>
              </Button>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

const CartItem = ({
  item: {
    title,
    cardImage,
    quantity,
    price,
    selectedPreferredSize: size,
    selectedPreferredColor: color,
    id,
  },
}) => {
  const { addItem, updateItemQuantity, removeItem } = useCart();

  const formattedProductIdWithParams = `${id.split("/")[0]}?${new URLSearchParams({
    size,
    color,
  })}`;
  const productColor = PRODUCT_COLORS_TO_NAMES[color];
  return (
    <div className="cart-item">
      <button
        onClick={() => removeItem(id)}
        className="cart-item__delete"
        aria-label="Видалити"
      >
        <IconDelete />
      </button>
      <div className="cart-item__thumb">
        <img src={cardImage || imgCartItem} alt="product title" />
      </div>
      <div className="cart-item__body">
        <Link to={`${ROUTES.PRODUCTS}/${formattedProductIdWithParams}`}>
          <h3 className="cart-item__title c-text-18">{title}</h3>
        </Link>

        <div className="cart-item__controls">
          <p className="cart-item__price">Ціна: {price} грн</p>
          <p className="cart-item__price">Кількість: {quantity}</p>
          <p className="cart-item__price">
            Сума: {Number(price) * quantity} грн
          </p>
          <p className="cart-item__price">
            Розмір: <span className="uppercase"> {size}</span>
          </p>
          <p className="cart-item__price">
            Колір: <span className="uppercase">{productColor}</span>
          </p>
          <div className="cart-item__wrapper">
            <button
              onClick={() => updateItemQuantity(id, quantity - 1)}
              disabled={quantity === 1}
              className="cart-item__button cart-item__button--minus"
            >
              <IconMinus />
            </button>
            <button
              onClick={() =>
                addItem(
                  {
                    id,
                    title,
                    cardImage,
                    quantity,
                    price,
                    size,
                    color,
                  },
                  1
                )
              }
              className="cart-item__button cart-item__button--plus"
            >
              <IconPlus />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cart;
