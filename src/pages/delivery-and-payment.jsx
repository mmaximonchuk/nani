import React from "react";
import { Link } from "gatsby";

import { Button, Layout } from "../components";
import { ROUTES } from "../constants/routes";

import IconArrow from "../assets/images/svg/slider-arrow.inline.svg";
import imgHorizontalRect1 from "../assets/images/firstRect.png";
import imgVerticalRect1 from "../assets/images/verticalRect.png";

import "../assets/styles/pages/about.scss";

const About = () => {
  return (
    <Layout>
      <div className="about-page">
        <section className="first-section">
          <div className="container">
            <Link to={ROUTES.HOMEPAGE} className="c-text-14 first-section__btn">
              <IconArrow className="first-section__arrow" /> Повернутись
            </Link>
          </div>
        </section>
        <section className="second-section">
          <div className="container">
            <div className="text-block mb-64">
              <h1 className="c-title-26 mb-36">Доставка:</h1>
              <p className="c-text-18 mb-24">
                Терміни виготовлення та відправки замовлення займають орієнтовно
                3-5 робочих днів з моменту оплати, в залежності від
                завантаженості виробництва. Терміни можуть бути змінені, про що
                ми попереджаємо клієнта завчасно.
              </p>
              <p className="c-text-18 mb-24">
                Доставляємо по території України та по всьому світу. Доставка по
                Україні здійснюється через транспортні компанії: Нова Пошта та
                Укрпошта. Вартість доставки залежить від тарифів компанії,
                відповідно до місця доставки та розміру посилки.
              </p>{" "}
              <p className="c-text-18">
                Послуги з доставки сплачує покупець. При замовленні від 2-ох
                одиниць товару для клієнта діє пропозиція безкоштовної доставки
                в межах України.
              </p>
            </div>

            <div className="text-block mb-64">
              <h2 className="c-title-26 mb-36">Оплата:</h2>
              <p className="c-text-18 mb-24">
                Ми працюємо за повною передоплатою, після чого ваше замовлення
                відправляється на виробництво. Оплата здійснюється за
                реквізитами:
              </p>
              <p className="c-text-18">
                ФОП Стовбира А.Ю. <br /> ІПН (ЄДРПОУ): 3679507742 <br /> Номер
                рахунку IBAN: UA903220010000026004330080412
              </p>
            </div>

            <div className="text-block">
              <h2 className="c-title-26 mb-36">Повернення та обмін:</h2>
              <p className="c-text-18">
              Згідно з ЗУ, повернення/обмін протягом 2 тижнів…
              </p>
            </div>
          </div>
        </section>
      </div>
    </Layout>
  );
};

export default About;
