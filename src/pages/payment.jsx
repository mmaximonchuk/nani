import React from "react";
import { Link } from "gatsby";

import { Button, InputGroup, Layout } from "../components";

import { ROUTES } from "../constants/routes";

import IconArrow from "../assets/images/svg/slider-arrow.inline.svg";
import IconApple from "../assets/images/svg/apple.inline.svg";

import "../assets/styles/pages/payment.scss";

const Payment = () => {
  return (
    <Layout title="Сплатити замовлення">
      <div className="payment-page">
        <section className="first-section">
          <div className="container">
            <Link to={ROUTES.HOMEPAGE} className="c-text-14 first-section__btn">
              <IconArrow className="first-section__arrow" /> Повернутись
            </Link>
          </div>
        </section>
        <section className="second-section">
          <div className="container">
            <h1 className="c-title-48">сплатити замовлення</h1>

            <form className="payment-form">
              <div className="payment-form__card-number payment-form__card-number--w-100">
                <p className="payment-form__label">Номер картки:</p>
                <InputGroup
                  htmlType="text"
                  name="cardNumber"
                  wrapperClassName="payment-form__card"
                  lableClassName="payment-form__input-label"
                  placeholder="ХХХХ ХХХХ ХХХХ ХХХХ"
                  inputClassName="payment-form__input"
                  placeholderClassName="payment-form__placeholder"
                />
              </div>
              <div className="payment-form__details">
                <div className="payment-form__card-number">
                  <p className="payment-form__label">Строк дії:</p>
                  <InputGroup
                    htmlType="text"
                    name="cardExpires"
                    wrapperClassName="payment-form__expires"
                    lableClassName="payment-form__input-label"
                    placeholderClassName="payment-form__placeholder"
                    inputClassName="payment-form__input"
                    placeholder="ХХ/ХХ"
                  />
                </div>
                <div className="payment-form__card-number payment-form__card-number--cvv">
                  <p className="payment-form__label">CVV:</p>
                  <InputGroup
                    htmlType="text"
                    name="cardCvv"
                    wrapperClassName="payment-form__cvv"
                    lableClassName="payment-form__input-label"
                    inputClassName="payment-form__input"
                    placeholderClassName="payment-form__placeholder"
                    placeholder="ХХХ"
                  />
                </div>
              </div>
              <div className="payment-form__controls">
                <Button variant="primary" className="payment-form__btn">
                  <Link to={ROUTES.THANK_YOU}>сплатити замовлення</Link>
                </Button>
                <p>або</p>
                <Button variant="primary" className="payment-form__btn">
                  Apple Pay <IconApple />
                </Button>
              </div>
            </form>
          </div>
        </section>
      </div>
    </Layout>
  );
};

export default Payment;
