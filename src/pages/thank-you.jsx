import React, { useEffect, useState } from "react";
import { Button, Layout } from "../components";
import { Link } from "gatsby";
import { ROUTES } from "../constants/routes";

import "../assets/styles/pages/thank-you-page.scss";
import toast from "react-hot-toast";

async function checkPaymentStatus(
  invoiceId,
  token
) {
  const url = `https://api.monobank.ua/api/merchant/invoice/status?invoiceId=${invoiceId}`;

  try {
    const response = await fetch(url, {
      method: "GET",
      headers: {
        "X-Token": token,
      },
    });

    const result = await response.json();
    console.log("Статус платежа:", result.status);

    return result;
  } catch (error) {
    console.error("Ошибка проверки статуса:", error);
  }
}

const PAYMENT_STATUS_TO_MESSAGE = {
  success: "Оплата пройшла успішно",
  canceled: "Оплата відмінена",
  pending: "Очікується оплата",
};

const ThankYou = () => {
  const [paymentStatus, setPaymentStatus] = useState(
    PAYMENT_STATUS_TO_MESSAGE.pending
  );

  useEffect(() => {
    const invoiceId = localStorage.getItem("invoiceId");
    console.log("invoiceId: ", invoiceId);
    if (!invoiceId) {
      return;
    }

    checkPaymentStatus(invoiceId, process.env.GATSBY_API_MONOBANK_API_TOKEN).then((result) => {
      setPaymentStatus(result.status);
      if (result.status === "success") {
        localStorage.removeItem("invoiceId");
        toast("Платіж пройшов успішно!", { icon: "🎉" });
      }
    });
  }, []);

  return (
    <Layout>
      <div className="thank-you-page">
        <section className="second-section">
          <div className="container">
            <div className="row">
              <div className="col section__wrapper">
                <h1 className="c-title-48">{PAYMENT_STATUS_TO_MESSAGE[paymentStatus]}</h1>
                {paymentStatus === "success" && (
                  <p className="c-title-32">
                    Наш менеджер зв'яжеться з вами протягом 24 годин, очікуйте
                    на відповідь.
                  </p>
                )}
                <Button className="second-section__btn" variant="secondary">
                  <Link to={ROUTES.HOMEPAGE}>Купити в один клік</Link>
                </Button>
              </div>
            </div>
          </div>
        </section>
      </div>
    </Layout>
  );
};

export default ThankYou;
