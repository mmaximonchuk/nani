import React from "react";

import { Layout, CatalogComponent } from "../components";

import { categoryTypeToName } from "../constants/categoryTypeToName";

import "../assets/styles/pages/catalog.scss";

function CatalogPage({ pageContext: { data, productType }, location }) {
  const title = categoryTypeToName[productType];

  return (
    <Layout title={title}>
      <CatalogComponent data={data} />
    </Layout>
  );
}

export default CatalogPage;
