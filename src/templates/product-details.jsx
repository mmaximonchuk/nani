import React, { useState } from "react";
import * as prismicH from "@prismicio/helpers";
import { useCart } from "react-use-cart";
import { navigate } from "gatsby";

import {
  ProductDetails,
  Layout,
  ProductGallery,
  // ProductCard,
  GoBackSection,
} from "../components";

import { ROUTES } from "../constants/routes";

// import IconArrow from "../assets/images/svg/slider-arrow.inline.svg";
// import imgLogo from "../assets/images/logo.png";

import "../assets/styles/pages/product.scss";
import { PRODUCT_COLORS, PRODUCT_SIZES } from "../constants/product-types";
import SimilarProducts from "../components/SimilarProducts";
import toast from "react-hot-toast";

const backRoute = {
  hoodie: ROUTES.CATALOG_HOODIE,
  tshirt: ROUTES.CATALOG_TSHIRT,
};

const Product = ({
  pageContext: {
    product: {
      id,
      type,
      title,
      // thumbs,
      gallery,
      price,
      density,
      material,
      is_in_white,
      is_in_black,
      // card_image,
    },
  },
}) => {
  const { addItem, inCart, updateItemQuantity, getItem } = useCart();

  // const product = {
  //   id,
  //   type,
  //   title,
  //   price,
  //   gallery,
  //   density,
  //   material,
  //   is_in_white,
  //   is_in_black,
  //   card_image,
  // };
  // console.log(product);
  const [galleryItems, setGalleryItems] = useState(() =>
    gallery.map((item) => ({
      original: item.gallery_image.url,
      thumbnail: item.gallery_image.url,
      originalWidth: 440,
      color: item.color,
    }))
  );
  const [selectedPreferredColor, setSelectedPreferredColor] = useState(() => {
    const params = new URLSearchParams(
      typeof window !== "undefined" && window.location.search
    );
    const color = params.get("color");

    if (color) return color;

    return PRODUCT_COLORS[0];
  });
  const [selectedPreferredSize, setSelectedPreferredSize] = useState(() => {
    const params = new URLSearchParams(
      typeof window !== "undefined" && window.location.search
    );
    const size = params.get("size");

    if (size) return size;

    return PRODUCT_SIZES[0];
  });

  const onSelectPreferredColor = (color) => {
    setSelectedPreferredColor(color);
  };

  const onSelectPreferredSize = (size) => {
    setSelectedPreferredSize(size);
  };

  const foundIndex = galleryItems.findIndex(
    (item) => item.color === selectedPreferredColor
  );
  const startIndex = foundIndex === -1 ? 0 : foundIndex;

  const onAddItem = () => {
    const addToCartImage = gallery[startIndex].gallery_image;
    const cardImageURL = prismicH.asImageSrc(addToCartImage);
    const cartItem = getItem(id);

    if (
      inCart(id) &&
      cartItem.selectedPreferredSize === selectedPreferredSize
    ) {
      updateItemQuantity(id, cartItem.quantity + 1);
      return;
    }

    addItem(
      {
        id: `${id}/${selectedPreferredColor}/${selectedPreferredSize}`,
        type,
        title: title.text,
        price: price.text,
        selectedPreferredColor,
        selectedPreferredSize,
        cardImage: cardImageURL,
      },
      1
    );

    toast("Товар додано до кошика", { icon: "🛒" });
  };

  const onBuyInOneClick = () => {
    onAddItem();
    navigate(ROUTES.ORDER);
  };

  return (
    <Layout title={title.text} descritpion={title.text}>
      <div className="product-page">
        <GoBackSection backTo={backRoute[type]} />
        <div className="container">
          <div className="content row">
            <ProductGallery
              galleryItems={galleryItems}
              startIndex={startIndex}
            />
            <ProductDetails
              id={id}
              type={type}
              title={title}
              price={price}
              density={density}
              material={material}
              isInWhite={is_in_white}
              isInBlack={is_in_black}
              onAddItem={onAddItem}
              onBuyInOneClick={onBuyInOneClick}
              onSelectPreferredColor={onSelectPreferredColor}
              selectedPreferredColor={selectedPreferredColor}
              selectedPreferredSize={selectedPreferredSize}
              onSelectPreferredSize={onSelectPreferredSize}
            />
          </div>
          <SimilarProducts productPageId={id} type={type} />
        </div>
      </div>
    </Layout>
  );
};

export default Product;
