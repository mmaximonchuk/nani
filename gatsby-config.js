const { linkResolver } = require("./config/prismic/link-resolver");
const path = require("path");

require("dotenv").config({
  path: `.env`,
});

module.exports = {
  siteMetadata: {
    title: `naniukraine`,
    siteUrl: `https://www.instagram.com/naniukraine/?igshid=YmM0MjE2YWMzOA%3D%3D`,
    social: {
      instagram: "naniukraine",
    },
  },
  plugins: [
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sass",
    "gatsby-plugin-image",
    "gatsby-plugin-sitemap",
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        icon: "src/images/icon.png",
      },
    },
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /\.inline\.svg$/,
        },
      },
    },

    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: "gatsby-source-prismic",
      options: {
        repositoryName: "nani-ukraine",
        schemas: {
          product: require("./src/schemas/product.json"),
        },
        accessToken:
          "MC5aaHE1aUJFQUFCNEFwajlj.XSLvv73vv71D77-9Rjnvv71777-9TiUmWe-_ve-_vWXvv71c77-9Y2cxESjvv70KXu-_vRPvv70",
        imageImgixParams: {
          auto: "compress,format",
          fit: "max",
          q: 95,
        },
      },
    },
  
  ],
};
